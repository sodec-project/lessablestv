<?php

namespace App\Console\Commands;

use App\HelpersClass\YoutubeHelper;
use App\Model\Video;
use App\Repository\Video\VideoRepository;
use Illuminate\Console\Command;

class ImportYoutubeVideo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:youtube';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importe les vidéos de youtube';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \DailymotionAuthRequiredException
     */
    public function handle()
    {
        $dailymotion = new \Dailymotion();
        // Connexion
        $dailymotion->setGrantType(\Dailymotion::GRANT_TYPE_PASSWORD,
            config('dailymotion.client_id'),
            config('dailymotion.client_secret'),
            [config('dailymotion.scope')],
            [
                "username" => config('dailymotion.username'),
                "password" => config('dailymotion.password')
            ]);

        $res = $dailymotion->get('/me/videos', [
            "fields" => ['id', 'title', 'description', 'thumbnail_240_url', 'channel', 'embed_url']
        ]);

        foreach ($res['list'] as $list) {
            Video::create([
                "youtube_id"    => $list['embed_url'],
                "title"         => $list['title'],
                "description"   => $list['description'],
                "tag"           => $list['channel'],
                "images"        => $list['thumbnail_240_url']
            ]);
        }

    }
}
