<?php

namespace App\Http\Controllers\Api\News;

use App\HelpersClass\Rss;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;use Illuminate\Support\Carbon;

class NewsController extends BaseController
{

    public function __construct()
    {
    }

    public function loadBref() {
        $datas = Rss::getFlux();
        ob_start();
        ?>
        <?php foreach ($datas['articles'] as $article): ?>
        <div class="row mt-lg-2">
            <div class="col-md-1"><i class="flaticon-time"></i> </div>
            <div class="col-md-2"><?= Carbon::createFromTimestamp(strtotime($article['publishedAt']))->format('H:i') ?></div>
            <div class="col-md-9"><?= $article['title']; ?></div>
        </div>
        <?php endforeach; ?>
        <?php
        $content = ob_get_clean();

        return $this->sendResponse($content, "Derni");
    }

    public function loadBrefNazaire() {
        $datas = Rss::rssFeed();
        ob_start();
        ?>
        <?php foreach ($datas as $article): ?>
            <ul class="list-unstyled">
                <li><a href="<?= str_replace('<![CDATA[', '', $article[1]); ?>"><?= str_replace('<![CDATA[', '', $article[0]); ?></a></li>
            </ul>
        <?php endforeach; ?>
        <?php
        $content = ob_get_clean();

        return $this->sendResponse($content, "Derni");
    }
}
