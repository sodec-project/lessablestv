<?php

namespace App\Http\Controllers\Api\Video;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Repository\Video\VideoRepository;
use Illuminate\Http\Request;

class VideoController extends BaseController
{
    /**
     * @var VideoRepository
     */
    private $videoRepository;

    /**
     * VideoController constructor.
     * @param VideoRepository $videoRepository
     */
    public function __construct(VideoRepository $videoRepository)
    {
        $this->videoRepository = $videoRepository;
    }

    public function loadVideoCategory($category_id)
    {
        $datas = $this->videoRepository->allForCategory($category_id);
        ob_start();
        ?>
        <?php foreach ($datas as $data): ?>
        <div>
            <div class="card ml-2">
                <a href="<?= $data->youtube_id; ?>"><img src="<?= $data->images; ?>" class="card-img-top" /> </a>
                <div class="card-body-floting">
                    <h5 class="card-body-floting--text"><?= $data->title; ?></h5>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        <?php
        $content = ob_get_clean();

        return $this->sendResponse($content, "OK");
    }
}
