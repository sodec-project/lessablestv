<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repository\Video\VideoRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @var VideoRepository
     */
    private $videoRepository;

    /**
     * HomeController constructor.
     * @param VideoRepository $videoRepository
     */
    public function __construct(VideoRepository $videoRepository)
    {
        $this->videoRepository = $videoRepository;
    }

    public function index()
    {
        return view("index", [
            "peoples" => $this->videoRepository->allForCategory(22),
            "sports" => $this->videoRepository->allForCategory(17),
            "sciences" => $this->videoRepository->allForCategory(28),
            "activities" => $this->videoRepository->allForCategory(29),
            "interviews" => $this->videoRepository->allForCategory(1),
            "educations" => $this->videoRepository->allForCategory(27),
            "divertissements" => $this->videoRepository->allForCategory(24),
            "musics" => $this->videoRepository->allForCategory(10),
            "politics" => $this->videoRepository->allForCategory(25),
        ]);
    }
}
