<?php

namespace App\Http\Controllers;


class TestController extends Controller
{
    public function code()
    {
        $daily = new \Dailymotion();
        try {
            $daily->setGrantType(
                \Dailymotion::GRANT_TYPE_PASSWORD,
                config('dailymotion.client_id'),
                config('dailymotion.client_secret'),
                [config('dailymotion.scope')],
                [
                    "username" => config('dailymotion.username'),
                    "password" => config('dailymotion.password')
                ]);
        } catch (\DailymotionAuthRequiredException $e) {
            dd($e->getMessage());
        }
        $res = $daily->get('/me/videos', [
            "fields" => ['id', 'title', 'description', 'thumbnail_240_url', 'channel']
        ]);

        dd($res);
    }
}
