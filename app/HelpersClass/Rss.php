<?php


namespace App\HelpersClass;


use Illuminate\Support\Facades\Http;

class Rss
{
    public static function getFlux()
    {
        $tt = Http::get('http://newsapi.org/v2/top-headlines?country=fr&apiKey='.env("NEWSAPI_KEY"));
        return $tt->json();
    }

    public static function rssFeed()
    {
        $file = "https://lessablesdolonne.maville.com/flux/rss/actu.php?xtor=RSS-18&c=loc&code=sn";
        $objets = ["title", "link"];
        if($chaine = @implode("",@file($file))) {

            // on découpe la chaine obtenue en items
            $tmp = preg_split("/<\/?"."item".">/",$chaine);

            // pour chaque item
            for($i=1;$i<sizeof($tmp)-1;$i+=2)

                // on lit chaque objet de l'item
                foreach($objets as $objet) {

                    // on découpe la chaine pour obtenir le contenu de l'objet
                    $tmp2 = preg_split("/<\/?".$objet.">/",$tmp[$i]);

                    // on ajoute le contenu de l'objet au tableau resultat
                    $resultat[$i-1][] = @$tmp2[1];
                }

            // on retourne le tableau resultat
            return $resultat;
        }
    }
}
