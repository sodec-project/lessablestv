<?php


namespace App\HelpersClass;


use Alaouy\Youtube\Facades\Youtube;
use App\Model\Video;

class YoutubeHelper
{
    private $client;

    public function __construct()
    {
        $this->client = new \Google_Client();
        $this->client->setDeveloperKey(env("YOUTUBE_API_KEY"));
    }

    public function get_datas($token = null)
    {
        $result = collect();
        $service = new \Google_Service_YouTube($this->client);

        if ($token != null) {
            $params = [
                "channelId" => env("YOUTUBE_CHANNEL_ID"),
                "order" => 'date',
                "pageToken" => $token,
            ];
        } else {
            $params = [
                "channelId" => env("YOUTUBE_CHANNEL_ID"),
                "order" => 'date',
            ];
        }


        return $service->search->listSearch('id,snippet', $params);
    }

    public function loop($datas)
    {
        $service = new \Google_Service_YouTube($this->client);
        foreach ($datas['items'] as $video) {
            if ($video['id']['kind'] == 'youtube#video') {
                $infos = $service->videos->listVideos('id,snippet', ["id" => $video['id']['videoId']]);
                if($infos['items'][0]['snippet']['thumbnails']['maxres']['url']){$images = $infos['items'][0]['snippet']['thumbnails']['maxres']['url'];}else{$images = '/storage/logo.png';}
                $table = new Video();
                $table->newQuery()->create([
                    "youtube_id" => $video['id']['videoId'],
                    "title" => $video['snippet']['title'],
                    "description" => $video['snippet']['title'],
                    "tag" => $infos['items'][0]['snippet']['categoryId'],
                    "images" => $images
                ]);
            }
        }
    }

    public function scrape($datas)
    {
        if ($datas['nextPageToken']) {
            $this->nextPage($datas['nextPageToken']);
        }
    }

    public function nextPage($token)
    {
        $next_data = $this->get_datas($token);
        $this->loop($next_data);
        $this->scrape($next_data);
    }
}
