<?php
namespace App\Repository\Video;

use App\Model\Video;

class VideoRepository
{
    /**
     * @var Video
     */
    private $video;

    /**
     * VideoRepository constructor.
     * @param Video $video
     */

    public function __construct(Video $video)
    {
        $this->video = $video;
    }

    public function allForPaginate()
    {
        return $this->video->newQuery()
            ->orderBy('id', 'desc')
            ->paginate();
    }

    public function allForCategory($category)
    {
        return $this->video->newQuery()
            ->orderBy('id', 'desc')
            ->where('tag', $category)
            ->get();
    }

}

