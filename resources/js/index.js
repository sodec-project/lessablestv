import * as $ from "jquery";
const slick = require('slick-carousel/slick/slick.min');

function loadBref() {
    let div = $("#loadBref");

    KTApp.block(div);

    $.get('/api/news/loadBref')
        .done((data) => {
            KTApp.unprogress();
            div.html(data.data)
        })
}

function loadBrefNazaire() {
    let div = $("#loadBrefNazaire");

    KTApp.block(div);

    $.get('/api/news/loadBrefNazaire')
        .done((data) => {
            KTApp.unprogress();
            div.html(data.data)
        })
}

function loadPeople() {
    let element = $("#slidePeople");

    KTApp.block(element);

    $.get('/api/video/category/22')
        .done((data) => {
            element.html(data.data);
            element.slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                verticalSwiping: true,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })
        })
}

function loadSport() {
    let element = $("#slideSport");

    KTApp.block(element);

    $.get('/api/video/category/'+element.attr('data-id'))
        .done((data) => {
            element.html(data.data);
            element.slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                verticalSwiping: true,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })
        })
}

function loadScience() {
    let element = $("#slideScience");

    KTApp.block(element);

    $.get('/api/video/category/'+element.attr('data-id'))
        .done((data) => {
            element.html(data.data);
            element.slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                verticalSwiping: true,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })
        })
}
function loadActivities() {
    let element = $("#slideActivities");

    KTApp.block(element);

    $.get('/api/video/category/'+element.attr('data-id'))
        .done((data) => {
            element.html(data.data);
            element.slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                verticalSwiping: true,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })
        })
}
function loadInterview() {
    let element = $("#slideInterview");

    KTApp.block(element);

    $.get('/api/video/category/'+element.attr('data-id'))
        .done((data) => {
            element.html(data.data);
            element.slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                verticalSwiping: true,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })
        })
}
function loadEducation() {
    let element = $("#slideEducation");

    KTApp.block(element);

    $.get('/api/video/category/'+element.attr('data-id'))
        .done((data) => {
            element.html(data.data);
            element.slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                verticalSwiping: true,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })
        })
}
function loadDiver() {
    let element = $("#slideDivertissement");

    KTApp.block(element);

    $.get('/api/video/category/'+element.attr('data-id'))
        .done((data) => {
            element.html(data.data);
            element.slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                verticalSwiping: true,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })
        })
}
function loadMusic() {
    let element = $("#slideMusics");

    KTApp.block(element);

    $.get('/api/video/category/'+element.attr('data-id'))
        .done((data) => {
            element.html(data.data);
            element.slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                verticalSwiping: true,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })
        })
}
function loadPolitics() {
    let element = $("#slidePolitics");

    KTApp.block(element);

    $.get('/api/video/category/'+element.attr('data-id'))
        .done((data) => {
            element.html(data.data);
            element.slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                verticalSwiping: true,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })
        })
}

function loadPoliticss() {
    let element = $("#slidePoliticss");

    KTApp.block(element);

    $.get('/api/video/category/'+element.attr('data-id'))
        .done((data) => {
            element.html(data.data);
            element.slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                verticalSwiping: true,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })
        })
}

function countdownTimer() {
    const difference = +new Date("2020-11-08 13:02:00") - +new Date();
    let remaining = "Time's up!";

    if (difference > 0) {
        const parts = {
            jours: Math.floor(difference / (1000 * 60 * 60 * 24)),
            heures: Math.floor((difference / (1000 * 60 * 60)) % 24),
            minutes: Math.floor((difference / 1000 / 60) % 60),
            secondes: Math.floor((difference / 1000) % 60)
        };

        remaining = Object.keys(parts)
            .map(part => {
                if (!parts[part]) return;
                return `<strong>${parts[part]}</strong> ${part}`;
            })
            .join(" ");
    }

    document.getElementById("timer").innerHTML = remaining;
}

countdownTimer();

loadBref();
loadBrefNazaire();
loadBrefNazaire();
loadPeople();
loadSport();
loadScience();
loadActivities();
loadInterview();
loadEducation();
loadDiver();
loadMusic();
loadPolitics();
loadPoliticss();

jQuery(document).ready(function ($) {
    $('#example1').sliderPro({
        width: 'auto',
        height: 500,
        arrows: true,
        buttons: false,
        waitForLayers: true,
        thumbnailWidth: 200,
        thumbnailHeight: 100,
        thumbnailPointer: true,
        autoplay: false,
        autoScaleLayers: false,
        breakpoints: {
            500: {
                thumbnailWidth: 120,
                thumbnailHeight: 50
            }
        }
    });

    setInterval(countdownTimer, 1000);
});
