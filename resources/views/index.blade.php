@extends("layout.front")

@section("style")
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
@endsection

@section("subheader")
@endsection

@section("content")
    <!--<div class="alert alert-info fade show" role="alert">
        <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
        <div class="alert-text">L'équipe ST Nazaire TV informe que le site est actuellement en cours de modernisation, les nouvelles fonctionnalités seront disponibles prochainement.<br>
            Nous vous remercions de votre fidélité.
        </div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="la la-close"></i></span>
            </button>
        </div>
    </div>-->
    <div id="carouselExampleInterval" class="carousel slide mt-lg-3 mb-lg-3" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active" data-interval="4000" >
                <img src="/storage/prochainement.png" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item" data-interval="4000" onclick="window.location.href='https://www.lessablesdolonne.fr/'">
                <img src="/storage/infos-utiles.png" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="/storage/numero-utiles.png" class="d-block w-100" alt="...">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="container-fluid mt-lg-3 mb-lg-3">
        <div class="card" style="background: rgb(2,0,36);
background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgb(39, 198, 250) 50%, rgb(251, 176, 26) 100%);">
            <div class="card-body">
                <h3 class="title kt-font-light"><span class="iconify" data-inline="false" data-icon="si-glyph:podium"></span> Campagne Municipal Les Sables d'Olonne 2020</h3>
                <div id="slidePoliticss" data-id="3"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <h2><span class="iconify" data-inline="false" data-icon="ic:round-slow-motion-video"
                      style="font-size: 60px; color: #1C804A"></span> Vidéo à la une</h2>
            <div class="card">
                <a href="https://www.youtube.com/watch?v=qoErYzRetsg"><img src="https://i.ytimg.com/vi/qoErYzRetsg/maxresdefault.jpg" class="card-img-top" /> </a>
                <div class="card-body-floting">
                    <h5 class="card-body-floting--text">Commémoration de la Tragédie SNSM Les Sables d'Olonne en hommage aux sauveteurs disparus</h5>
                </div>
            </div>
            <div class="divider"></div>
            <div class="container mt-lg-3">
                <div class="card" style="background: rgb(2,0,36);
background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgb(39, 198, 250) 50%, rgb(251, 176, 26) 100%);">
                    <div class="card-body">
                        <h3 class="title kt-font-light"><span class="iconify" data-inline="false" data-icon="si-glyph:podium"></span> Vie Politique</h3>
                        <div id="slidePolitics" data-id="25"></div>
                    </div>
                </div>
            </div>
            <div class="container mt-lg-3">
                <div class="card" style="background: rgb(2,0,36);
background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgb(39, 198, 250) 50%, rgb(251, 176, 26) 100%);">
                    <div class="card-body">
                        <h3 class="title kt-font-light"><span class="iconify" data-inline="false" data-icon="icomoon-free:newspaper"></span> Société</h3>
                        <div id="slidePeople" data-id="22"></div>
                    </div>
                </div>
            </div>
            <div class="container mt-lg-3">
                <div class="card" style="background: rgb(2,0,36);
background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgb(39, 198, 250) 50%, rgb(251, 176, 26) 100%);">
                    <div class="card-body">
                        <h3 class="title kt-font-light"><span class="iconify" data-inline="false" data-icon="ic:baseline-sports-soccer"></span> Sports</h3>
                        <div id="slideSport" data-id="17"></div>
                    </div>
                </div>
            </div>
            <div class="container mt-lg-3">
                <div class="card" style="background: rgb(2,0,36);
background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgb(39, 198, 250) 50%, rgb(251, 176, 26) 100%);">
                    <div class="card-body">
                        <h3 class="title kt-font-light"><span class="iconify" data-inline="false" data-icon="entypo:lab-flask"></span> Sciences</h3>
                        <div id="slideScience" data-id="28"></div>
                    </div>
                </div>
            </div>
            <div class="container mt-lg-3">
                <div class="card" style="background: rgb(2,0,36);
background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgb(39, 198, 250) 50%, rgb(251, 176, 26) 100%);">
                    <div class="card-body">
                        <h3 class="title kt-font-light"><span class="iconify" data-inline="false" data-icon="fa-solid:hand-holding"></span> Vie Associative</h3>
                        <div id="slideActivities" data-id="29"></div>
                    </div>
                </div>
            </div>
            <!--<div class="container mt-lg-3">
                <div class="card" style="background: rgb(2,0,36);
background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgb(39, 198, 250) 50%, rgb(251, 176, 26) 100%);">
                    <div class="card-body">
                        <h3 class="title kt-font-light"><span class="iconify" data-inline="false" data-icon="typcn:microphone"></span> Interviews</h3>
                        <div id="slideInterview" data-id="1"></div>
                    </div>
                </div>
            </div>-->
            <!--<div class="container mt-lg-3">
                <div class="card" style="background: rgb(2,0,36);
background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgb(39, 198, 250) 50%, rgb(251, 176, 26) 100%);">
                    <div class="card-body">
                        <h3 class="title kt-font-light"><span class="iconify" data-inline="false" data-icon="ion:school-sharp"></span> Educations</h3>
                        <div id="slideEducation" data-id="27"></div>
                    </div>
                </div>
            </div>-->
            <div class="container mt-lg-3">
                <div class="card" style="background: rgb(2,0,36);
background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgb(39, 198, 250) 50%, rgb(251, 176, 26) 100%);">
                    <div class="card-body">
                        <h3 class="title kt-font-light"><span class="iconify" data-inline="false" data-icon="maki:cinema-11"></span> Divertissement</h3>
                        <div id="slideDivertissement" data-id="24"></div>
                    </div>
                </div>
            </div>
            <div class="container mt-lg-3">
                <div class="card" style="background: rgb(2,0,36);
background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgb(39, 198, 250) 50%, rgb(251, 176, 26) 100%);">
                    <div class="card-body">
                        <h3 class="title kt-font-light"><span class="iconify" data-inline="false" data-icon="bx:bxs-music"></span> Musiques</h3>
                        <div id="slideMusics" data-id="10"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">

            <div class="card">
                <video controls poster="https://resabilletcse.com/storage/home/video-0.png" class="card-img-top">
                    <source src="https://resabilletcse.com/storage/other/video_0.mp4" type="video/mp4">
                </video>
                <a href="https://francebilletreduc.shop">
                    <div class="card-body" style="background: rgb(255,111,0);background: linear-gradient(90deg, rgba(255,111,0,1) 0%, rgba(250,184,0,1) 100%); color: white">
                        <h2 class="card-title text-center">RESABILLET PRO</h2>
                        <p style="font-weight:bold; text-align: center; font-size: 14px;">TPE - PME - PMI - ARTISAN - COMMERCANT: RENDEZ FORTEMENT VISIBLE VOTRE ENSEIGNE & ENTREPRISE AUPRES DES SALARIES + AYANTS DROITS DE L’ENSEMBLE DES COMITES D’ENTREPRISES SOCIAUX ECONOMIQUES, MAIRIES ET COLLECTIVITES EN FRANCE</p>
                    </div>
                </a>
            </div>
            <div class="mt-lg-5"></div>
            <!--<div class="card">
                <div class="card-header" style="background: rgb(2,0,36);
background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgb(39, 198, 250) 50%, rgb(251, 176, 26) 100%); color: white">
                    <h3>Informations Utiles</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <strong>Mairie des Sables d'Olonne</strong><br>
                            CS 21842<br>
                            21, place du Poilu de France<br>
                            85118 Les Sables d'Olonne cedex
                        </div>
                        <div class="col-md-6">
                            <strong>02 51 23 16 00</strong>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <strong>MAIRIE ANNEXE D'OLONNE SUR MER</strong><br>
                            Rue des Sables<br>
                            85340 Les Sables d'Olonne
                        </div>
                        <div class="col-md-6">
                            <strong>02 51 23 16 00</strong>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <strong>MAIRIE ANNEXE DU CHÂTEAU D'OLONNE</strong><br>
                            53, rue Séraphin Buton<br>
                            85180 Les Sables d'Olonne
                        </div>
                        <div class="col-md-6">
                            <strong>02 51 23 16 00</strong>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <strong>MAIRIE ANNEXE DE LA CHAUME - AGENCE POSTALE COMMUNALE</strong><br>
                            Quai Rousseau Méchin<br>
                            85108 Les Sables d'Olonne
                        </div>
                        <div class="col-md-6">
                            <strong>02 51 23 16 00</strong>
                        </div>
                    </div>
                </div>
            </div>-->
            <div class="mt-lg-5"></div>
            <h2><span class="iconify" data-inline="false" data-icon="icomoon-free:newspaper"
                      style="font-size: 60px; color: #1C804A"></span> En Bref - Les Sables d'Olonne</h2>
            <div class="card">
                <div class="card-body" id="loadBrefNazaire"></div>
            </div>
            <div class="mt-lg-5"></div>
            <h2><span class="iconify" data-inline="false" data-icon="icomoon-free:newspaper"
                      style="font-size: 60px; color: #1C804A"></span> En Bref - National</h2>
            <div class="card">
                <div class="card-body" id="loadBref"></div>
            </div>
            <div class="mt-lg-5"></div>
            <div class="card">
                <video controls poster="https://resabilletcse.com/storage/home/video.png" class="card-img-top">
                    <source src="https://resabilletcse.com/storage/other/video.mp4" type="video/mp4">
                </video>
                <a href="https://francebilletreduc.shop">
                    <div class="card-body" style="background: rgb(255,111,0);background: linear-gradient(90deg, rgba(255,111,0,1) 0%, rgba(250,184,0,1) 100%); color: white">
                        <h2 class="card-title text-center">FRANCEBILLETREDUC</h2>
                        <p style="font-weight:bold; text-align: center; font-size: 14px;">PARTICULIER: PROFITEZ DE REDUCTIONS & AVANTAGES SOCIAUX AUX MEME TITRE QUE TOUT SALARIES + AYANTS DROITS DES COMITES D’ENTREPRISES SOCIAUX ECONOMIQUES, MAIRIES ET COLLECTIVITES EN FRANCE</p>
                    </div>
                </a>
            </div>
            <div class="mt-lg-5"></div>
        </div>
    </div>
    <!--<div id="example1" class="slider-pro">
        <div class="sp-slides">
            <div class="sp-slide" onclick="window.location.href='https://www.youtube.com/watch?v=SmyyHM4F3Sg&feature=youtu.be'">
                <img class="sp-image" src="https://i.ytimg.com/vi/SmyyHM4F3Sg/maxresdefault.jpg"
                     data-src="https://i.ytimg.com/vi/SmyyHM4F3Sg/maxresdefault.jpg"
                     data-retina="https://i.ytimg.com/vi/SmyyHM4F3Sg/maxresdefault.jpg"/>
            </div>
            <div class="sp-slide" onclick="window.location.href='https://actu.fr/pays-de-la-loire/sables-dolonne_85194/municipales-2020-aux-sables-d-olonne-ce-qu-il-faut-savoir-sur-l-organisation-du-second-tour_34461518.html'">
                <img class="sp-image" src="https://static.actu.fr/uploads/2020/06/img-3255-2.jpg"
                     data-src="https://static.actu.fr/uploads/2020/06/img-3255-2.jpg"
                     data-retina="https://static.actu.fr/uploads/2020/06/img-3255-2.jpg"/>
            </div>
            <div class="sp-slide">
                <img class="sp-image" src="/storage/slideshow/3.jpg"
                     data-src="/storage/slideshow/3.jpg"
                     data-retina="/storage/slideshow/3.jpg"/>
            </div>
        </div>

        <div class="sp-thumbnails">
            <div class="sp-thumbnail">
                <div class="sp-thumbnail-title">Municipales 2020 aux Sables-d'Olonne : débat d'avant second tour Moreau-Tesson-Legrand-Pottier</div>
            </div>
            <div class="sp-thumbnail">
                <div class="sp-thumbnail-title">Municipales 2020 aux Sables-d’Olonne : ce qu’il faut savoir sur l’organisation du second tour</div>
            </div>
            <div class="sp-thumbnail">
                <div class="sp-thumbnail-title">Respect des gestes barrières dans les bars et restos : aux Sables-d’Olonne, les clients jouent (presque) tous le jeu</div>
            </div>
        </div>
    </div>-->
@endsection

@section("script")
    <script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
    <script src="{{ asset('js/index.js') }}"></script>
@endsection
