<!--<div id="kt-header" class="kt-header kt-grid__item" data-ktheader-minimize="on">
    <div class="kt-header__bottom" style="height: 150px;">
        <div class="kt-container--fluid " style="background: #ffdb87; width: 100%; height: 100%">
            <div style="text-align: center; margin-top: 10px;">
                <div class="row">
                    <div class="col-md-1">&nbsp;</div>
                    <div class="col-md-4" style="margin-top: 30px;">
                        <img src="/storage/france_billet_logo_long.png" width="300" class="img-fluid">
                        <p style="font-size: 15px;">Contact: 09 72 42 21 85<br><a href="mailto:contact@resabilletcse.com">contact@resabilletcse.com</a></p>

                    </div>
                    <div class="col-md-3">
                        <p style="font-size: 18px;font-weight:bold;">FRANCEBILLETREDUC - RESABILLET PRO</p>
                        <p style="font-size: 13px; font-weight:bold;">TPE - PME - PMI - ARTISAN - COMMERCANT: RENDEZ FORTEMENT VISIBLE VOTRE ENSEIGNE & ENTREPRISE AUPRES DES SALARIES + AYANTS DROITS DE L’ENSEMBLE DES COMITES D’ENTREPRISES SOCIAUX ECONOMIQUES, MAIRIES ET COLLECTIVITES EN FRANCE</p>
                    </div>
                    <div class="col-md-3" style="margin-top: 30px;">
                        <a href="https://francebilletreduc.shop" class="btn btn-outline-brand btn-lg">En savoir Plus...</a>
                    </div>
                    <div class="col-md-1">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</div>-->
<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " data-ktheader-minimize="off">

    <div class="kt-header__top">
        <div class="kt-container ">

            <!-- begin:: Brand -->
            <div class="kt-header__brand  kt-grid__item" id="kt_header_brand">
                <div class="kt-header__brand-logo">
                    <a href="{{ route('Front.index') }}">
                        <img alt="Logo" src="/storage/logo.png" width="80"/>
                    </a>
                    <span style="font-size: 35px; font-weight: bold;position: relative;top: -15px;left: 10px;">LES SABLES AGGLO TV</span>
                </div>
            </div>

            <!-- end:: Brand -->
        </div>
    </div>
    <div class="kt-header__bottom">
        <div class="kt-container ">

            <!-- begin: Header Menu -->
            <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
            <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
                <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
                    <ul class="kt-menu__nav ">
                        <li class="kt-menu__item {{ \App\HelpersClass\Generator::currentRoute(route('Front.index')) }} kt-menu__item--rel">
                            <a href="{{ route('Front.index') }}" class="kt-menu__link">
                                <span class="kt-menu__link-icon la la-mobile-phone"  style="color: #646c9a"></span>
                                <span class="kt-menu__link-text">Acceuil</span>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </a>
                        </li>
                        <li class="kt-menu__item kt-menu__item--rel" data-toggle="kt-tooltip" title="Bientôt Disponible">
                            <a href="#" class="kt-menu__link">
                                <span class="kt-menu__link-text">Catégorie</span>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </a>
                        </li>
                        <li class="kt-menu__item kt-menu__item--rel" data-toggle="kt-tooltip" title="Bientôt Disponible">
                            <a href="#" class="kt-menu__link">
                                <span class="kt-menu__link-text">Presse</span>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </a>
                        </li>
                        <li class="kt-menu__item kt-menu__item--rel" data-toggle="kt-tooltip" title="Bientôt Disponible">
                            <a href="#" class="kt-menu__link">
                                <span class="kt-menu__link-text">Communication Professionnel</span>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </a>
                        </li>
                        <li class="kt-menu__item kt-menu__item--rel" data-toggle="kt-tooltip" title="Bientôt Disponible">
                            <a href="#" class="kt-menu__link">
                                <span class="kt-menu__link-text">Annuaire Professionnel</span>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </a>
                        </li>
                        <li class="kt-menu__item kt-menu__item--rel" data-toggle="kt-tooltip" title="Bientôt Disponible">
                            <a href="#" class="kt-menu__link">
                                <span class="kt-menu__link-text">Contact</span>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="kt-header-toolbar">
                    <button class="btn btn-pill btn-elevate btn-danger">LIVE</button>
                </div>
            </div>

            <!-- end: Header Menu -->
        </div>
    </div>
    <div class="kt-header__bottom">
        <div class="kt-container ">

            <!-- begin: Header Menu -->
            <nav class="navbar navbar-expand-lg kt-header-menu-wrapper">
                <a class="navbar-brand" href="#">
                    <img src="/storage/globe.svg" width="80" height="80" alt="" loading="lazy">
                </a>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">

                    </ul>
                    <div class="kt-header-toolbar">
                        Prochain départ dans &nbsp; <div id="timer"></div>
                    </div>
                </div>
            </nav>

            <!-- end: Header Menu -->
        </div>
    </div>
</div>
