<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(["prefix" => "news", "namespace" => "Api\News"], function (){
    Route::get('loadBref', "NewsController@loadBref");
    Route::get('loadBrefNazaire', "NewsController@loadBrefNazaire");
});
Route::group(["prefix" => "video", "namespace" => "Api\Video"], function (){
    Route::get('category/{category_id}', "VideoController@loadVideoCategory");
});
